<?php
/**
 * Plugin Name: Cookie Choices
 * Author: Mattia Migliorini
 * Author URI: http://deshack.net
 * Description: Minimal code to make sure your website conforms to the Italian Cookie Law
 * Version: 1.0.0
 * Text Domain: wp-cookie-choices
 * License: GPLv2 or later
 *
 * @package WPCookieChoices
 * @copyright 2015 VB Italia Srl
 * @since 1.0.0
 */

// If this file is called directly, abort.
if ( ! defined('ABSPATH') )
	die;

if ( ! function_exists('array_insert') ) :
/**
 * Insert into array at the specified position.
 *
 * @since 1.0.0
 *
 * @param array $array The array to insert element into.
 * @param mixed $el The element to insert.
 * @param int $pos The position in which to insert the element.
 * @return array The resulting array.
 */
function array_insert( array $array, $el, $pos ) {
	$start = array_splice($array, 0, $pos, array($el));
	return array_merge($start, $array);
}
endif;

if ( ! class_exists('WPCookieChoices') ) :
/**
 * Cookie Choices for WordPress.
 *
 * @package WPCookieChoices
 * @author Mattia Migliorini <mattia@vbitalia.net>
 * @since 1.0.0
 * @version 1.0.0
 */
class WPCookieChoices {
	/**
	 * Plugin version.
	 *
	 * @since 1.0.0
	 * @access public
	 * @var string
	 */
	const VERSION = '1.0.0';

	/**
	 * Plugin slug.
	 *
	 * Used as text domain.
	 *
	 * @since 1.0.0
	 * @access public
	 * @var string
	 */
	const SLUG = 'wp-cookie-choices';

	/**
	 * Capability necessary to handle plugin settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 * @var string
	 */
	protected $capability = 'manage_options';

	/**
	 * Options list.
	 *
	 * @since 1.0.0
	 * @access private
	 * @var array
	 */
	private $options = array();

	/**
	 * Cookie name.
	 *
	 * @since 1.0.0
	 * @access private
	 * @var string
	 */
	private $cookieName = 'displayCookieConsent';

	/**
	 * Cookie value.
	 *
	 * @since 1.0.0
	 * @access private
	 * @var string
	 */
	private $cookieVal = 'y';

	/**
	 * Metabox ID.
	 *
	 * @since 1.0.0
	 * @access private
	 * @var string
	 */
	private $metabox_id;

	/**
	 * User preferences.
	 *
	 * @since 1.0.0
	 * @access private
	 * @static
	 * @var array
	 */
	private static $preferences = array();

	/**
	 * Constructor.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function __construct() {
		$this->metabox_id = self::SLUG . '-options';
		/**
		 * Add admin menu page.
		 */
		add_action('admin_menu', array($this, 'addMenuPage'));

		// Initialize plugin.
		add_action('init', array($this, 'init'));
		add_action('init', array($this, 'addCmb2Class'), 9999);
		add_action('cmb2_init', array($this, 'add_options_page_metabox'));

		/**
		 * Print script in footer.
		 */
		add_action('wp_footer', array($this, 'printScriptInline'), 9);

		// Load components based on user cookie preferences.
		$this->handleCookies();
	}

	/**
	 * Load components based on user cookie preferences.
	 *
	 * If you want to filter the tracking code inserted by your own theme or plugin,
	 * just pass it through the wpcc_google_analytics filter.
	 *
	 * @example
	 * ```
	 * apply_filters('wpcc_google_analytics', $tracking_code);
	 * ```
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function handleCookies() {
		if ( !self::acceptCookie('analytics') ) {
			add_filter('yoast-ga-push-array-universal', function($gaq_push) {
				$gaq_push = array_filter($gaq_push, array($this, 'filterYoastGaqPush'));
				// Anonymize IPs if not done yet.
				if ( ! in_array("'set', 'anonymizeIp', true", $gaq_push) )
					$gaq_push = array_insert($gaq_push, "'set', 'anonymizeIp', true", 1);
				return $gaq_push;
			});
			add_filter('wpcc_google_analytics', function($code) {
				$code = preg_replace('/[^;]+displayfeatures(.*;)/', '', $code);
				if ( false === strpos($code, 'anonymizeIp') )
					$code = preg_replace('/([^;]+)[(][^;]+create(.*;)/', "$0$1('set','anonymizeIp',true);", $code);
				return $code;
			});
		}
	}

	/**
	 * Filter commands to push in Google Analytics by Yoast.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @var string|array $el Single element from the `$gaq_push` array.
	 * @return bool
	 */
	public function filterYoastGaqPush($el) {
		if ( strpos($el, 'displayfeatures') !== false )
			return false;
		return true;
	}

	/**
	 * Add settings page.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function addMenuPage() {
		add_options_page(
			__('Cookie Choices Dashboard', self::SLUG),
			__('Cookie Choices', self::SLUG),
			$this->capability,
			self::SLUG,
			array($this, 'dashboard')
		);
	}

	/**
	 * Load CMB2.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function addCmb2Class() {
		if ( ! class_exists('CMB2') )
			require_once plugin_dir_path(__FILE__) . 'vendor/CMB2/init.php';
	}

	/**
	 * Add options metabox to the array of metaboxes.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function add_options_page_metabox() {
		$cmb = new_cmb2_box( array(
			'id'      => $this->metabox_id,
			'hookup'  => false,
			'show_on' => array(
				'key'   => 'options-page',
				'value' => array( self::SLUG )
			)
		));

		// Set CMB2 fields.

		// Activation
		$cmb->add_field( array(
			'name' => __('Activate', self::SLUG),
			'desc' => __('Activate banner', self::SLUG),
			'id' => 'active',
			'type' => 'checkbox'
		));
		// Banner type
		$cmb->add_field(array(
			'name' => __('Banner type', self::SLUG),
			'desc' => __('How to display the banner', self::SLUG),
			'id' => 'type',
			'type' => 'select',
			'options' => array(
				'bar' => __('Top Bar', self::SLUG),
				'dialog' => __('Dialog', self::SLUG)
			)
		));
		// Banner text
		$cmb->add_field(array(
			'name' => __('Text', self::SLUG),
			'desc' => __('Text to display in the banner', self::SLUG),
			'id' => 'text',
			'type' => 'textarea_small'
		));
		// Cookie Policy URL
		$cmb->add_field(array(
			'name' => __('Cookie Policy URL', self::SLUG),
			'id' => 'url',
			'type' => 'text_url',
			'protocol' => array('http','https')
		));
		// Cookie Policy anchor text
		$cmb->add_field(array(
			'name' => __('Anchor Text', self::SLUG),
			'desc' => __('Text for the cookie policy button', self::SLUG),
			'id' => 'anchor_text',
			'type' => 'text_small',
			'default' => __('Policy', self::SLUG)
		));
		// Accept button text
		$cmb->add_field(array(
			'name' => __('Accept button text', self::SLUG),
			'desc' => __('Text for the accept button', self::SLUG),
			'id' => 'button_text',
			'type' => 'text_small',
			'default' => __('OK', self::SLUG)
		));
	}

	/**
	 * Dashboard callback.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function dashboard() {
		if ( ! current_user_can($this->capability) )
			wp_die( __('You do not have sufficient permissions to access this page.') );

		?>
<script>
	//Required for multi CMB form
	jQuery(document).ready(function($) {
		jQuery('.cmb-form #wp_meta_box_nonce').appendTo('.cmb-form');
	});
</script>

<div class="wrap">

	<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>

	<?php cmb2_metabox_form( $this->metabox_id, self::SLUG ); ?>
</div>
		<?php
	}

	/**
	 * Initialize settings.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function init() {
		/**
		 * Load plugin textdomain.
		 */
		load_plugin_textdomain(self::SLUG, false, dirname(plugin_basename(__FILE__)) . '/i18n/');
	}

	/**
	 * Output script in footer.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function printScriptInline() {
		// Exit early if not active.
		if ( ! self::get_option('active'))
			return;

		// Select type of banner to display.
		if ( $type = self::get_option('type') )
			$type = ucfirst($type);

		$json_encode = (function_exists('wp_json_encode')) ? 'wp_json_encode' : 'json_encode';

		// Snippet to display the banner.
		$banner = sprintf(
			'document.addEventListener("DOMContentLoaded", function(event){cookieChoices.showCookieConsent%1$s(%2$s,"%3$s","%4$s","%5$s");});',
			$type,
			$json_encode(self::get_option('text')),
			esc_js(self::get_option('button_text')),
			esc_js(self::get_option('anchor_text')),
			esc_url(self::get_option('url'))
		);

		// Noscript snippet in case the browser has JavaScript disabled.
		$noscript = sprintf(
			'<noscript><style>html{margin-top:35px}</style><div id="cookieChoiceInfo"><span>%1$s</span><a href="%2$s" target="_blank" style="margin-left:8px;" rel="nofollow">%3$s</a><a id="cookieChoiceDismiss" href="#" style="margin-left:24px;display:none;">%4$s</a></div></div></noscript>',
			$json_encode(self::get_option('text')),
			esc_url(self::get_option('url')),
			esc_js(self::get_option('anchor_text')),
			esc_js(self::get_option('button_text'))
		);

		$style = '#cookieChoiceInfo{top:0;left:0;position:absolute;width:100%;min-height:50px;padding:13px!important;text-align:center;z-index:9999;background-color:#5a5a5a!important;color:#fff!important;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}#cookieChoiceInfo span{font-size:14px;color:#fff;line-height:24px;}#cookieChoiceInfo a{display:inline-block;font-weight:700;text-align:center;vertical-align:middle;touch-action:manipulation;cursor:pointer;background-image:none;border:none;white-space:nowrap;border-radius:2px;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;text-transform:uppercase;-webkit-transition:background-color 0.2s,color 0.2s;-moz-transition:background-color 0.2s,color 0.2s;-o-transition:background-color 0.2s,color 0.2s;-ms-transition:background-color 0.2s,color 0.2s;transition:background-color 0.2s,color 0.2s;text-decoration:none;background-color:transparent;border:1px solid #f9f9f9;color:#f9f9f9;font-size:10px;padding:4px 6px;line-height:14px;}#cookieChoiceInfo a:focus{outline:thin dotted;outline:5px auto -webkit-focus-ring-color;outline-offset:-2px;}#cookieChoiceInfo a:hover,#cookieChoiceInfo a:focus{color:#333;background-color:#f9f9f9;}';

		// Output code.
		echo '<!-- WP Cookie Choices --><style>body.admin-bar #cookieChoiceInfo{top:32px!important;}'.$style.'</style><script>';
		require 'lib/cookiechoices.php';
		echo $banner . '</script>' . $noscript;
	}

	/**
	 * Retrieve option.
	 *
	 * @since 1.0.0
	 * @access public
	 * @static
	 *
	 * @param string $key Optional. Options array key. Default empty.
	 * @return mixed Option value.
	 */
	public static function get_option($key = '') {
		if ( function_exists('cmb2_get_option') )
			return apply_filters('wpcc-get-option', cmb2_get_option(self::SLUG, $key), $key);
		trigger_error('CMB2 library not loaded yet!', E_USER_WARNING);
		return false;
	}

	/**
	 * Whether the user accepted cookies.
	 *
	 * @since 1.0.0
	 * @access public
	 * @static
	 *
	 * @param string $preference Optional. User preference. Default empty (all).
	 * @return bool
	 */
	public static function acceptCookie($preference = '') {
		if ( defined('DOING_WP_CRON') && DOING_WP_CRON )
			return true;
		if ( ! isset($_COOKIE['displayCookieConsent']))
			return false;
		if ( !empty($preference) && isset(self::$preferences[$preference]) )
			return self::$preferences[$preference];
		return true;
	}
}
endif;

new WPCookieChoices;
