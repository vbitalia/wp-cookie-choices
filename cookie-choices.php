<?php
/**
 * Cookie Choices.
 *
 * Let your website comply with the Cookie Law.
 *
 * @package CookieChoices
 * @copyright 2015 VB Italia Srl
 * @author Mattia Migliorini <mattia@vbitalia.net>
 * @since 1.0.0
 */

/**
 * Cookie Choices class.
 *
 * @package CookieChoices
 * @copyright 2015 VB Italia Srl
 * @author Mattia Migliorini <mattia@vbitalia.net>
 * @since 1.0.0
 * @version 1.0.0
 */
class CookieChoices {
	/**
	 * Cookie banner types.
	 *
	 * @since 1.0.0
	 * @access protected
	 * @var string
	 */
	const TYPE_BAR = 'Bar';
	const TYPE_DIALOG = 'Dialog';

	/**
	 * Selected banner type.
	 *
	 * @since 1.0.0
	 * @access protected
	 * @var string
	 */
	protected $type = '';

	/**
	 * Banner text.
	 *
	 * @since 1.0.0
	 * @access protected
	 * @var string
	 */
	protected $text;

	/**
	 * Button text.
	 *
	 * @since 1.0.0
	 * @access protected
	 * @var string
	 */
	protected $button_text;

	/**
	 * Last instance of this class.
	 *
	 * @since 1.0.0
	 * @access protected
	 * @static
	 * @var CookieChoices
	 */
	protected static $instance = null;

	/**
	 * Constructor.
	 *
	 * Sets up required object properties.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @param string $text Banner text.
	 * @param string $button_text Button text.
	 * @param string $link_text Link text.
	 * @param string $url URL.
	 * @param string $type Optional. Banner type. Default TYPE_BAR.
	 */
	public function __construct( $text, $button_text, $link_text, $url, $type = self::TYPE_BAR ) {
		if ( ! is_string( $text ) )
			throw new InvalidArgumentException('The first argument of CookieChoices::__construct() must be a string!');
		$this->text = stripslashes($text);

		if ( ! is_string( $button_text ) )
			throw new InvalidArgumentException('The second argument of CookieChoices::__construct() must be a string!');
		$this->button_text = stripslashes($button_text);

		if ( ! is_string( $link_text ) )
			throw new InvalidArgumentException('The third argument of CookieChoices::__construct() must be a string!');
		$this->link_text = stripslashes($link_text);

		$this->url = $url;

		if ( ! ( self::TYPE_BAR === $type || self::TYPE_DIALOG === $type ) ) {
			trigger_error('The fifth argument of CookieChoices::__construct() must be one of: TYPE_BAR, TYPE_DIALOG.', E_USER_WARNING);
			$type = self::TYPE_BAR;
		}

		$this->type = $type;

		$this->instance = $this;
	}

	/**
	 * Retrieve last created instance of this object.
	 *
	 * Note: you should make sure you already instantiated the class. This would
	 * return null otherwise.
	 *
	 * @since 1.0.0
	 * @access public
	 * @static
	 *
	 * @return CookieChoices
	 */
	public static function getInstance() {
		return self::$instance;
	}

	/**
	 * Output JavaScript code for the banner.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function outputCode() {
		?>
<script type="text/javascript">
<?php require 'lib/cookiechoices.php'; ?>
document.addEventListener('DOMContentLoaded', function(event) {
	cookieChoices.showCookieConsent<?php echo $this->type; ?>(<?php echo json_encode($this->text); ?>,
		'<?php echo $this->button_text; ?>',
		'<?php echo $this->link_text; ?>',
		'<?php echo $this->url; ?>'
	);
});
</script>
		<?php
	}
}
