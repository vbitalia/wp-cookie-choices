module.exports = function(grunt) {
	'use strict';

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		jshint: {
			all: [
				'Gruntfile.js',
				'lib/*.js'
			]
		},

		uglify: {
			dist: {
				files: {
					'lib/cookiechoices.php': [
						'lib/cookiechoices.js'
					]
				}
			}
		},

		version: {
			php: {
				options: {
					prefix: '@version\\s*'
				},
				src: ['cookie-choices.php']
			},
			wp: {
				options: {
					prefix: '@version\\s*'
				},
				src: ['wp-cookie-choices.php']
			},
			class: {
				options: {
					prefix: 'VERSION\\s*'
				},
				src: ['wp-cookie-choices.php']
			},
			bower: {
				options: {
					prefix: 'version\\s*'
				},
				src: ['bower.json']
			}
		},

		watch: {
			js: {
				files: ['lib/*.js','Gruntfile.js'],
				tasks: ['test','build']
			}
		}
	});

	require('load-grunt-tasks')(grunt);

	grunt.registerTask('default', ['watch']);
	grunt.registerTask('deploy', ['test','build','version']);
	grunt.registerTask('test', ['jshint']);
	grunt.registerTask('build', ['uglify']);

	grunt.event.on('watch', function(action, filepath) {
		grunt.log.writeln(filepath + ' has ' + action);
	});
};
